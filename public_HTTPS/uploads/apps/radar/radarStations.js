// list of US weather radar sites to view

SAGE2_radarStations = [];
SAGE2_radarStations[0] = {code:"LOT",
						name:"Chi"};
SAGE2_radarStations[1] = {code:"HMO",
						name:"Haw"};
SAGE2_radarStations[2] = {code:"NKX",
						name:"San D"};
SAGE2_radarStations[3] = {code:"OKX",
						name:"New Y"};
SAGE2_radarStations[4] = {code:"GRK",
						name:"Aus"};

/*
    SAGE2_radarStations = [
        "LOT", // Chicago
        "HMO", // Honolulu
        "NKX", // San Diego
        "OKX", // New York City
        "GRK" // Austin
    ];
    */